<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Models\Chat;
use App\Models\Message;
use App\Models\Read;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index(Request $request)
    {
        return Message::query()
            ->where('chat_id', $request->chat_id)
            ->with('reads')
            ->latest()
            ->simplePaginate(15);
    }

    public function store(Request $request)
    {
        $chatId = $request->chat_id;

        Chat::getChatById($chatId);

//        if (!$chat) {
//            return response()->json([
//                'error' => 'Chat not found',
//            ], 404);
//        }

        $message = $request->body;

        event(new MessageSent());

        try {
            Message::create([
                'from_id' => auth()->id(),
                'chat_id' => $chatId,
                'body'    => $message,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
            ], 500);
        }

        return $message;
    }

    public function markAsRead($id)
    {
        return Read::create([
            'user_id' => auth()->id(),
            'message_id' => $id,
            'read_at' => now(),
        ]);
    }
}
