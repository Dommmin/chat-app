import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import Layout from "./components/Layout.jsx";
import Index from "./Index.jsx";
import Login from "./auth/Login.jsx";
import Register from "./auth/Register.jsx";
import PageNotFound from "./components/PageNotFound.jsx";
import Pusher from "pusher-js";
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import ChatsShow from "./Chats/Show.jsx";
import ChatsIndex from "./Chats/Index.jsx";

window.Pusher = Pusher;

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'mykey',
//     cluster: 'mt1',
//     wsHost: 'localhost',
//     wsPort: 6002,
//     forceTLS: false,
//     disableStats: true,
//     enabledTransports: ['ws'],
// });

const queryClient = new QueryClient()


const router = createBrowserRouter([
    {
        path: '/',
        element: <Layout />,
        children: [
            {
                path: '',
                element: <Index />,
            },
            {
                path: '/chats',
                element: <ChatsIndex />,
            },
            {
                path: '/chats/:id',
                element: <ChatsShow />,
            },
        ],
    },
    {
        path: '/login',
        element: <Login />
    },
    {
        path: '/register',
        element: <Register />
    },
    {
        path: '*',
        element: <PageNotFound />
    }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
      <QueryClientProvider client={queryClient}>
          {/*<ReactQueryDevtools initialIsOpen={false} />*/}
          <RouterProvider router={router} />
      </QueryClientProvider>
  </React.StrictMode>,
)
