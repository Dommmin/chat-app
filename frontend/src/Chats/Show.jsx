import {useCallback, useEffect, useRef, useState} from "react";
import axios from "../lib/axios.js";
import { useAuth } from "../hooks/useAuth.js";
import {useInfiniteQuery, useQuery} from "@tanstack/react-query";
import {Link, useNavigate, useParams} from "react-router-dom";
import PageNotFound from "../components/PageNotFound.jsx";
import Message from "../components/Message.jsx";
import Echo from "laravel-echo";

function Index() {
    const { id } = useParams();
    const navigate = useNavigate();
    const [message, setMessage] = useState('');
    const { user, isLoading } = useAuth();
    const [error, setError] = useState('');
    
    const fetchChat = useCallback(() => {
        axios
            .get(`/api/chats/${id}`)
            .catch((error) => {
                if (error.response.status === 404) {
                    setError('Not Found');
                    console.log('Chat not found')
                }
            })
    }, [id]);

    useEffect(() => {
        fetchChat();
    }, [fetchChat]);
    
    const messagesEndRef = useRef(null);
    
    const fetchChats = async () => {
        const response = await axios.get('/api/chats');
        return response.data;
    };

    const { data: chats, isPending: isChatsPending, refetch: refetchChats } = useQuery({
        queryKey: ['chats'],
        queryFn: fetchChats,
    });

    const observerRef = useRef();
    const fetchMessages = useCallback(async ({ pageParam = 1 }) => {
        const response = await axios.get('/api/messages/', {
            params: {
                chat_id: id,
                page: pageParam
            }
        });
        return response.data;
    }, [id]);

    const {
        data: messages,
        isPending: isMessagesPending,
        refetch: refetchMessages,
        fetchNextPage,
        hasNextPage,
        isError,
    } = useInfiniteQuery({
        queryKey: ['messages', id],
        queryFn: fetchMessages,
        refetchOnWindowFocus: false,
        getNextPageParam: (lastPage) => lastPage.current_page + 1,
        enabled: !!id,
    });

    const topMessageRef = useCallback(
        (node) => {
            if (observerRef.current) observerRef.current.disconnect();
            observerRef.current = new IntersectionObserver(entries => {
                if (entries[0].isIntersecting && hasNextPage) {
                    fetchNextPage();
                }
            });
            if (node) observerRef.current.observe(node);
        },
        [fetchNextPage, hasNextPage]
    );

    const scrollToBottom = useCallback(() => {
        messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
    }, []);

    const scrollToAnchor = useCallback(() => {
        if (topMessageRef.current) {
            topMessageRef.current.scrollIntoView({ behavior: "smooth" });
        }
    }, [topMessageRef]);

    useEffect(() => {
        scrollToBottom();
        scrollToAnchor()
    }, [messages, scrollToAnchor, scrollToBottom]);

    useEffect(() => {
        refetchMessages();
    }, [refetchMessages, id]);

    const sendMessage = async (event) => {
        if (event.key !== 'Enter' || !message.trim() || !id) {
            return;
        }

        await setMessage('');

        try {
            await axios.post('/api/messages', {
                body: message,
                chat_id: id,
            });
        } catch (error) {
            if (error.response.status === 404) {
                console.log('Chat not found')
            }
        }

        await refetchMessages();
        await refetchChats();
    };

    useEffect(() => {
        const echo = new Echo({
            broadcaster: 'pusher',
            key: 'mykey',
            wsHost: 'localhost',
            cluster: 'mt1',
            wsPort: 6001,
            wssPort: 6001,
            transports: ['websocket'],
            enabledTransports: ['ws', 'wss'],
            forceTLS: false,
            disableStats: true
        });

        echo
            .channel('chat')
            .listen('.messages', async () => {
                console.log('new message');
                await refetchMessages();
                await refetchChats();
            });

        return () => {
            echo.disconnect();
        };
    }, [refetchChats, refetchMessages]);

    if (isLoading || isChatsPending) {
        return (
            <div className="h-[calc(100vh-4rem)] flex justify-center items-center">
                <span className="loading loading-spinner text-info"></span>
            </div>
        );
    }

    if (!user) {
        navigate('/login');
        return null;
    }

    if (isError) {
        return <PageNotFound />;
    }

    if (error) {
        return <PageNotFound />;
    }

    const handleClick = (latestMessage) => {
        if (!latestMessage.is_read) {
            console.log(true);
            axios
                .post('/api/messages/' + latestMessage.id + '/mark-as-read')
                .then(() => {
                    refetchChats();
                })
                .catch((err) => {
                    console.log(err);
                })
        }
    };

    return (
        <div className="h-[calc(100vh-4rem)] text-white flex bg-gray-700">
            <div className="hidden md:block h-full p-6 space-y-4 overflow-y-auto border-r border-gray-700">
                <button className="btn btn-primary btn-sm w-full">Group Chat</button>
                {isChatsPending
                    ? <span className="flex justify-center items-center mx-auto h-screen loading loading-spinner text-info"></span>
                    : chats.data.map((chat, index) => (
                        <Link
                            onClick={() => handleClick(chat.latest_message)}
                            to={'/chats/' + chat.id}
                            role="button" key={index}
                            className={`btn btn-lg grid grid-cols-6 gap-4 p-2 rounded-lg tooltip ${chat.id == id ? 'bg-default' : 'bg-gray-800'}`}
                            data-tip={chat.latest_message.created_at}
                        >
                            <div className="flex justify-center items-center">
                                <img src={chat.user.avatar} alt={chat.user.name} className="w-10 h-10 rounded-full"/>
                            </div>
                            <div className="col-span-4">
                                <div className={`text-sm ${!chat.latest_message.is_read ? 'font-bold text-white' : ''}`}>{chat.user.name}</div>
                                <div className="text-xs text-gray-500 truncate">
                                    {chat.latest_message.from_id === user.id && <span>You: </span>}
                                    {chat.latest_message.body}
                                </div>
                            </div>
                            {!chat.latest_message.is_read && <div className="badge badge-info badge-xs" />}
                        </Link>
                    ))}
            </div>

            <div className="w-full md:w-2/3 flex flex-col bg-gray-800 h-full">
                <div className="p-6 space-y-4 overflow-y-auto flex-grow">
                    {isMessagesPending
                        ? <span
                            className="flex justify-center items-center mx-auto h-[calc(100vh-16rem)] loading loading-spinner text-info"></span>
                        : <>
                            <div id="scroll-anchor"></div>
                            {messages.pages.slice().reverse().map((pageData, i) => (
                                pageData.data.slice().reverse().map((message, index) => {
                                    const isFirstMessage = i === 0 && index === 0;
                                    return (
                                        <Message
                                            ref={isFirstMessage ? topMessageRef : null}
                                            key={index}
                                            message={message}
                                            user={user}
                                        />
                                    )
                                })
                            ))}
                        </>
                    }
                    <div ref={messagesEndRef} />
                </div>

                <div className="p-10 sticky bottom-0">
                    <input
                        value={message}
                        type="text"
                        onChange={(event) => setMessage(event.target.value)}
                        onKeyDown={sendMessage}
                        className="w-full textarea textarea-info"
                        placeholder="Type a message..."
                    />
                </div>
            </div>
        </div>

    )
}

export default Index;
