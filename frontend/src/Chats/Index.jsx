import axios from "../lib/axios.js";
import {useCallback, useEffect, useState} from "react";

function Index() {
    const [searchTerm, setSearchTerm] = useState('');
    const [users, setUsers] = useState([]);
    const [name, setName] = useState('');
    const [isLoading, setIsLoading] = useState(true);

    const fetchUsers = useCallback(() => {
        axios
            .get('/api/users', {
                params: {
                    name
                }
            })
            .then((response) => {
                setUsers(response.data);
            })
            .catch((error) => {
                console.log(error);
            })
            .finally(() => {
                setIsLoading(false);
            })
    }, [name]);

    const handleSearch = async () => {
        setName(searchTerm);
    };

    useEffect(() => {
        fetchUsers();
    }, [fetchUsers]);

    return (
        <div className="flex justify-center items-center">
            <div className="w-full">
                <div className="flex justify-center items-center space-x-2 mt-5">
                    <label className="input input-bordered flex items-center gap-2">
                        <input onChange={(event) => setSearchTerm(event.target.value)} type="text" className="grow" placeholder="Search"/>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor"
                             className="w-4 h-4 opacity-70">
                            <path fillRule="evenodd"
                                  d="M9.965 11.026a5 5 0 1 1 1.06-1.06l2.755 2.754a.75.75 0 1 1-1.06 1.06l-2.755-2.754ZM10.5 7a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0Z"
                                  clipRule="evenodd"/>
                        </svg>
                    </label>
                    <button onClick={handleSearch} className="btn btn-outline btn-info">Search</button>
                </div>
                {isLoading
                    ? <div className="h-[calc(100vh-12rem)] flex justify-center items-center">
                            <span className="loading loading-spinner text-info"></span>
                        </div>
                    : <div className="flex justify-center items-center">
                        {users.length > 0
                            ? <button className="grid grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4 mt-5">
                                {users.map((user) => (
                                    <div key={user.id}
                                         className="btn btn-lg flex justify-start items-center p-2 rounded-lg bg-gray-800 tooltip">
                                        <div className="flex justify-center items-center col-span-1">
                                            <img src={user.avatar} alt={user.name}
                                                 className="w-10 h-10 rounded-full"/>
                                        </div>
                                        <div className="col-span-4">
                                            <div className="text-sm">{user.name}</div>
                                        </div>
                                    </div>
                                ))}
                                </button>
                            : <div className="flex justify-center items-center">
                                    <div className="text-center space-y-2">
                                        <p>No results found...</p>
                                    </div>
                                </div>
                        }
                    </div>
                }
            </div>
        </div>
        )
}

export default Index