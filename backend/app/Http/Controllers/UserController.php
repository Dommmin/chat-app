<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        return User::query()
            ->when($request->filled('name'), function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->name . '%');
            })
            ->when(!$request->filled('name'), function ($query) {
                $query->inRandomOrder()->limit(60);
            })
            ->get(['id', 'name', 'avatar']);
    }
}
