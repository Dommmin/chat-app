<?php

namespace App\Http\Controllers;

use App\Http\Resources\ChatResource;
use App\Models\Chat;
use Illuminate\Support\Facades\DB;

class ChatController extends Controller
{
    public function index()
    {
        $chats = Chat::select(['chats.name', 'chats.id', 'chats.is_group'])
            ->selectSub(function ($query) {
                $query->select('created_at')
                    ->from('messages')
                    ->whereColumn('messages.chat_id', 'chats.id')
                    ->orderByDesc('created_at')
                    ->limit(1);
            }, 'latest_message_date')
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('chat_user')
                    ->whereColumn('chats.id', 'chat_user.chat_id')
                    ->where('chat_user.user_id', auth()->id());
            })
            ->with([
                'latestMessage:id,from_id,body,attachment,chat_id,created_at',
                'latestMessage.reads' => function ($query) {
                    $query->where('user_id', auth()->id());
                },
                'users:id,name,avatar',
            ])
            ->orderByDesc('latest_message_date')
            ->simplePaginate(10);

        return ChatResource::collection($chats);
    }

    public function show($id)
    {
        $isNumeric = preg_match('/^\d+$/', $id);

        if (!$isNumeric) {
            return response()->json([
                'error' => 'Chat not found',
            ], 404);
        }

        $chat = Chat::getChatById($id);

        return new ChatResource($chat);
    }
}
