<?php

namespace App\Listeners;

use App\Events\MessageRead;
use App\Models\Read;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MessageReadListener implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(MessageRead $event): void
    {
        Read::create([
            'message_id' => $event->message->id,
            'user_id' => $event->userId,
            'read_at' => now(),
        ]);
    }
}
