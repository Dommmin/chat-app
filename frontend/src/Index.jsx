import axios from "./lib/axios.js";
import { useAuth } from "./hooks/useAuth.js";
import {Link, useNavigate} from "react-router-dom";
import {useEffect, useState} from "react";

function Index() {
    const [isPending, setIsPending] = useState(true)
    const [data, setData] = useState([]);
    const navigate = useNavigate();
    const { user, isLoading } = useAuth();

    const fetchChats = () => {
        axios
            .get('/api/chats')
            .then((response) => {
                setData(response.data.data);
            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setIsPending(false);
            })
    };

    useEffect(() => {
        fetchChats()
    }, [user]);

    useEffect(() => {
        if (data.length > 0) {
            navigate('/chats/' + data[0].id)
        }
    }, [data, navigate]);

    if (isLoading) {
        return (
            <div className="h-[calc(100vh-4rem)] flex justify-center items-center">
                <span className="loading loading-spinner text-info"></span>
            </div>
        );
    }

    if (!user) {
        return window.location.href = '/login';
    }

    if (isPending) {
        return (
            <div className="h-[calc(100vh-4rem)] flex justify-center items-center">
                <span className="loading loading-spinner text-info"></span>
            </div>
        );
    }

    return (
        <div className="flex justify-center items-center h-[calc(100vh-4rem)]">
            <div className="text-center space-y-2">
                <p>You don't have any chats yet...</p>
                <Link to="/chats" className="btn btn-info btn-outline">Search for chats</Link>
            </div>
        </div>
    )
}

export default Index;
