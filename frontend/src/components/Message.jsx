import React from "react";

const Message = React.forwardRef(({ message, user }, ref) => {
    const messageBody = (
        <div key={message.id}
             className={`flex items-end space-x-2 ${message.from_id === user.id ? 'justify-end' : ''}`}>
            <div
                className={`${message.from_id === user.id ? 'bg-blue-500' : 'bg-gray-700'} text-white rounded-2xl px-4 py-2 max-w-[250px] lg:max-w-lg`}>
                {message.body}
            </div>
        </div>
    )

    return ref
        ? <article ref={ref}>{messageBody}</article>
        : <article>{messageBody}</article>
})

export default Message;